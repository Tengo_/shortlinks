<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\LinksRepository")
 */
class Links
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Url()
     * @Assert\NotBlank
     */
    private $OriginalLink;

    /**
     * @ORM\Column(type="string", length=4, unique=true)
     */
    private $ShortLink;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShortLink(): ?string
    {
        return $this->ShortLink;
    }

    public function setShortLink(string $ShortLink): self
    {
        $this->ShortLink = $ShortLink;

        return $this;
    }

    public function getOriginalLink(): ?string
    {
        return $this->OriginalLink;
    }

    public function setOriginalLink(string $OriginalLink): self
    {
        $this->OriginalLink = $OriginalLink;

        return $this;
    }
}
