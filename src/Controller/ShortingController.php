<?php
// src/Controller/ShortingController.php
namespace App\Controller;


use App\Entity\Links;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ShortingController extends AbstractController
{   

    /**
	* @Route("/api/short")
    */

    public function short(ValidatorInterface $validator)
    {
        /*Для проверки отправляем запрос с помощью cURL:
          curl -X POST -H "Content-Type: application/json" -d '{"url":"http://yandex.ru"}' http://localhost:8000/api/short
        */

        $request = Request::createFromGlobals(); 
        $content = $request->getContent(); // вытаскиваем содержимое запроса
        $url = json_decode($content, true); // декодируем данные запроса
        $url = mb_strtolower($url['url']); // присваиваем значение из массива

        // здесь нужно написать проверку url, что это url 
        /*$state = $this->checkUrl($url);
        if (!$state) {
            throw $this->createNotFoundException(
                'То, что вы ввели не является корректным URL!'
            );
        }*/

        $checkLink = $this -> tryUrl($validator, $url); // вызываем функцию проверки валидности URL

        if (!$checkLink) {
            $response = new Response(); 
            $response->headers->set('Content-Type', 'text/html'); // указываем тип ответа
            $response->setContent('Введен не верный формат URL!');
            return $response;
        }


        // Проверяем есть ли такая ссылка в базе
        $entityManager = $this->getDoctrine()->getManager();
        $urlMatch = $entityManager->getRepository(Links::class)->findOneBy(['OriginalLink' => $url]); // Ищем запись с такой ссылкой
        
        // Если ссылки нет в базе
        if (!$urlMatch) {
        
            $urlShort = substr(strtoupper(md5($url)),0,4); // генерируем короткую ссылку

            // Проверяем на такую же короткую ссылку в базе
            if ($this->checkShortLink($urlShort)) {
                $urlShort = $this -> regenerateShortLink($url); // вызываем фунциию регенерации
            }
            
            // Вносим ссылки в базу данных
            $link = new Links(); //создаем экземпляр сущности
            $link->setOriginalLink($url); // присваиваем значение
            $link->setShortLink($urlShort);
            $entityManager->persist($link); // подготавливаем для добавления в базу
            $entityManager->flush();    
                   
        } else {
            // если ссылка в базе уже есть
            $urlShort = $urlMatch->getShortLink(); // получаем уже генерированную короткую ссылку
        }

        
        // Генерируем ответ
        $response = new Response(); 
        $response->setContent(json_encode([ // кодируем ответ в json
            'short' => $urlShort,
        ]));
        $response->headers->set('Content-Type', 'application/json'); // указываем тип документа
        return $response;

    /*
        $response = new Response();
        $response->setContent($url);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    */

    }



    /**
     * @Route("api/redirect/{token}")
     */

    public function redirectGo($token)
    {
        
        $link = $this->getDoctrine()
            ->getRepository(Links::class)
            ->findOneBy(['ShortLink' => $token]);

        if (!$link) {
            throw $this->createNotFoundException(
                'Нет адреса для короткой ссылки '.$token
            );
        }
        
        //return new Response('Оригинальная ссылка для короткой ссылки '.$token.': '.$link->getOriginalLink());
       $response = new RedirectResponse($link->getOriginalLink());
       return $response;
    }



    // Функция проверки валидности URL на основе регулярки (не использую)
    public function checkUrl($url)
    {
        $pattern = "/^(http|https):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|ru|us|tv|info|biz|su)$)?\/?/i";
        
        if(preg_match($pattern, $url)){
            return true;
            } else{
            return false;
            }
    }


    // Проверка на дубль короткой ссылки в базе
    public function checkShortLink($urlShort) {
        $entityManager = $this->getDoctrine()->getManager();
        $urlShortInBase = $entityManager->getRepository(Links::class)->findOneBy(['ShortLink' => $urlShort]);
        // Если ссылка найдена вернет true и нужно перегенерировать!
            if ($urlShortInBase) {
                return true;   
            } else {
                return false;
            }
    }
    

    // Генерирование короткой ссылки на случай совпадения с базой
    public function regenerateShortLink($url) {
        $salt = date('l jS \of F Y h:i:s A').rand(1,100);
        $urlShort = substr(strtoupper(md5($url.$salt)),0,4);
        if (!$this -> checkShortLink($urlShort)) {
            return $urlShort;
        } else {
            $this -> regenerateShortLink($url);
        }
        
    }



    /**
     * @Route("api/tryurl/{url}")
     */

    // Валидации URL с использованием Валидатора
    public function tryUrl(ValidatorInterface $validator, $url)
    {
        $link = new Links();
        $link -> setOriginalLink($url);
        

        $errors = $validator->validate($link);

        if (count($errors) > 0) {
            /*
             * Uses a __toString method on the $errors variable which is a
             * ConstraintViolationList object. This gives us a nice string
             * for debugging.
            */
            $errorsString = (string) $errors;

            return false;
        }

        return true;
    }


// --------------------------------------------------------------------------------------

    
}